using System;
using System.Reflection;

namespace Reflection
{
	class TypeExample
	{
		public static void Main (string[] args)
		{
			MyClass m = new MyClass ();
			Type type = m.GetType ();
			Console.WriteLine ("类型名:" + type.Name);
			Console.WriteLine ("类全名：" + type.FullName);
			Console.WriteLine ("命名空间名:" + type.Namespace);
			Console.WriteLine ("程序集名：" + type.Assembly);
			Console.WriteLine ("模块名:" + type.Module);
			Console.WriteLine ("基类名：" + type.BaseType);
			Console.WriteLine ("是否类：" + type.IsClass);
			Console.WriteLine ("类的公共成员：");
			MemberInfo[] memberInfos = type.GetMembers();//得到所有公共成员
			foreach (var item in memberInfos)
			{
				Console.WriteLine("{0}:{1}",item.MemberType,item);
			}
		}
	}

	class MyClass
	{
		public string m;

		public void test ()
		{
		}

		public int MyProperty { get; set; }
	}
}
