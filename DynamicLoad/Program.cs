using System;
using System.Reflection;

namespace Reflection
{
	class DynamicLoadExample
	{
		private int factor;

		public DynamicLoadExample (int f)
		{
			factor = f;
		}

		public int SampleMethod (int x)
		{
			Console.WriteLine ("\nDynamicLoadExample.SampleMethod({0}) executes.", x);
			return x * factor;
		}

		public static void Main (string[] args)
		{
			//获取当前执行代码的程序集
			Assembly assem = Assembly.GetExecutingAssembly ();

			Console.WriteLine ("Assembly Full Name:");
			Console.WriteLine (assem.FullName);

			// The AssemblyName type can be used to parse the full name.
			AssemblyName assemName = assem.GetName ();
			Console.WriteLine ("\nName: {0}", assemName.Name);
			Console.WriteLine ("Version: {0}.{1}",
				assemName.Version.Major, assemName.Version.Minor);
			Console.WriteLine ("\nAssembly CodeBase:");
			Console.WriteLine (assem.CodeBase);
			// 从程序集众创建一个Example实例并且用object类型的引用o指向它，同时调用一个输入参数的构造函数
			Object o = assem.CreateInstance ("Reflection.DynamicLoadExample", false,
				           BindingFlags.ExactBinding,
				           null, new Object[] { 2 }, null, null);

			//构造Example类的一个晚绑定的方法SampleMethod  
			MethodInfo m = assem.GetType ("Reflection.DynamicLoadExample").GetMethod ("SampleMethod");
			//调用刚才实例化好的Example对象o中的SampleMethod方法，传入的参数为42
			Object ret = m.Invoke (o, new Object[] { 42 });
			Console.WriteLine ("SampleMethod returned {0}.", ret);


			Console.WriteLine ("\nAssembly entry point:");
			Console.WriteLine (assem.EntryPoint);
		}
	}
}
