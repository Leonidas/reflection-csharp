using System;
using System.Reflection;

namespace Reflection
{
	class AssemblyExample
	{
		public static void Main (string[] args)
		{
			//获取当前执行代码的程序集
			Assembly assem = Assembly.GetExecutingAssembly();

			Console.WriteLine("程序集全名:"+assem.FullName);
			Console.WriteLine("程序集的版本："+assem.GetName().Version);
			Console.WriteLine("程序集初始位置:"+assem.CodeBase);
			Console.WriteLine("程序集位置："+assem.Location);
			Console.WriteLine("程序集入口："+assem.EntryPoint);


			Type[] types = assem.GetTypes();
			Console.WriteLine("程序集下包含的类型:");
			foreach (var item in types)
			{
				Console.WriteLine("类："+item.Name);
			}
		}
	}
}
